package oojava1.m02.exercise312;

public class PetrolPurchase {
	private String location; // Instance variable
	private String petrolType;
	private int    quantity;
	private double pricePerLitre;
	private double discount; // Discount in percent
	
	public PetrolPurchase(String location, String petrolType, int quantity, double pricePerLitre, double discount) {
		this.location = location;
		this.petrolType = petrolType;
		this.quantity = quantity;
		this.pricePerLitre = pricePerLitre;
		this.discount = discount;
	}
	
	public double getPurchaseAmount() {
		return quantity*pricePerLitre*(1-discount*0.01);
	}

	public String getPetrolType() {
		return petrolType;
	}

	public void setPetrolType(String petrolType) {
		this.petrolType = petrolType;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getPricePerLitre() {
		return pricePerLitre;
	}

	public void setPricePerLitre(double pricePerLitre) {
		this.pricePerLitre = pricePerLitre;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}
	
	

}
