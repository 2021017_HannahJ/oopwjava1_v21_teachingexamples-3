package oojava1.m03.ex03;

public class StaticTest {
	
	public static void main(String[] args) {
		double x, y;
		x = OurMath.add(3, 4);
		System.out.printf("Result of 3 + 4 = %f\n", x);
		x = OurMath.sub(3, 4);
		System.out.printf("Result of 3 - 4 = %f\n", x);
		Vector v1 = null;
		v1 = new Vector(3,0);
		Vector v2 = new Vector(4,0);
		x = Vector.addX(v1, v2);
		System.out.printf("Result of Vector.addX((3,0)+(4,0)) = %f\n",x );
		x=v1.getX();
		y=v1.getY();
		System.out.printf("The content of %s is (%f,%f)\n","v1",x,y);
		
		System.out.println("I am a Vector-object called v1 and this is my toString() representation: "+v1);

		x = Vector.add(v1, v2).getX();
		System.out.printf("Result of Vector.add((3,0)+(4,0)).getX() = %f\n", x );
		
		System.out.printf("Result : %d! = %d\n max2 %f\n max3: %f\n", 5, 
				OurMath.factorial(5),OurMath.max(2.3,4.5), OurMath.max(2.3,4.5,6.7) );
		
	}

}
